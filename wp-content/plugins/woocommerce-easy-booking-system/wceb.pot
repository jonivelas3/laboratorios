msgid ""
msgstr ""
"Project-Id-Version: WooCommerce Easy Booking\n"
"POT-Creation-Date: 2016-04-01 11:56+0100\n"
"PO-Revision-Date: 2016-04-01 11:56+0100\n"
"Last-Translator: \n"
"Language-Team: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Poedit 1.5.7\n"
"X-Poedit-KeywordsList: _;gettext;gettext_noop;__;_e\n"
"X-Poedit-Basepath: .\n"
"X-Poedit-SearchPath-0: .\n"

#: woocommerce-easy-booking.php:196
#: includes/settings/class-wceb-settings.php:140
#: includes/settings/class-wceb-settings.php:141
msgid "Settings"
msgstr ""

#: includes/class-wceb-ajax.php:247
msgid "Please choose valid dates"
msgstr ""

#: includes/class-wceb-ajax.php:250 includes/class-wceb-cart.php:64
msgid "Please choose two dates"
msgstr ""

#: includes/class-wceb-ajax.php:253
msgid "Please select product option"
msgstr ""

#: includes/class-wceb-ajax.php:256
msgid ""
"Please choose the quantity of items you wish to add to your cart&hellip;"
msgstr ""

#: includes/class-wceb-cart.php:182 includes/class-wceb-checkout.php:35
#: includes/class-wceb-checkout.php:67
#: includes/reports/class-wceb-list-reports.php:60
#: includes/reports/views/html-wceb-reports-filters.php:14
#: includes/settings/class-wceb-settings.php:28
msgid "Start"
msgstr ""

#: includes/class-wceb-cart.php:183 includes/class-wceb-checkout.php:36
#: includes/class-wceb-checkout.php:68
#: includes/reports/class-wceb-list-reports.php:61
#: includes/reports/views/html-wceb-reports-filters.php:18
#: includes/settings/class-wceb-settings.php:29
msgid "End"
msgstr ""

#: includes/class-wceb-product-archive-view.php:32
msgid "Select dates"
msgstr ""

#: includes/wceb-product-functions.php:199
msgid " / week"
msgstr ""

#: includes/wceb-product-functions.php:211
msgid " / night"
msgstr ""

#: includes/wceb-product-functions.php:215
msgid " / day"
msgstr ""

#: includes/admin/class-wceb-admin-assets.php:75
#: includes/admin/class-wceb-admin-assets.php:85
#: includes/admin/class-wceb-admin-assets.php:95
#: includes/admin/views/wceb-html-product-booking-options.php:50
#: includes/admin/views/wceb-html-product-booking-options.php:66
#: includes/admin/views/wceb-html-variation-booking-options.php:46
#: includes/admin/views/wceb-html-variation-booking-options.php:57
msgid "days"
msgstr ""

#: includes/admin/class-wceb-admin-assets.php:79
#: includes/admin/class-wceb-admin-assets.php:96
msgid "weeks"
msgstr ""

#: includes/admin/class-wceb-admin-assets.php:82
#: includes/admin/class-wceb-admin-assets.php:97
msgid "custom period"
msgstr ""

#: includes/admin/class-wceb-admin-product-settings.php:52
#: includes/admin/class-wceb-admin-product-settings.php:74
msgid "Bookable"
msgstr ""

#: includes/admin/class-wceb-admin-product-settings.php:53
msgid ""
"Bookable products can be rent or booked on a daily/weekly/custom schedule"
msgstr ""

#: includes/admin/class-wceb-admin-product-settings.php:104
#: includes/reports/views/html-wceb-reports.php:6
msgid "Bookings"
msgstr ""

#: includes/admin/class-wceb-admin-product-settings.php:207
msgid "Minimum booking duration must be inferior to maximum booking duration"
msgstr ""

#: includes/admin/views/wceb-html-notice-addons.php:11
msgid "Want more features for WooCommerce Easy Booking?"
msgstr ""

#: includes/admin/views/wceb-html-notice-addons.php:12
msgid " Check the add-ons!"
msgstr ""

#: includes/admin/views/wceb-html-product-booking-options.php:19
#: includes/admin/views/wceb-html-variation-booking-options.php:14
#: includes/settings/includes/wceb-general-settings.php:38
msgid "Booking duration"
msgstr ""

#: includes/admin/views/wceb-html-product-booking-options.php:21
#: includes/admin/views/wceb-html-variation-booking-options.php:15
msgid ""
"The booking duration of your products. Daily, weekly or a custom period (e."
"g. 28 days for a monthly booking). The price will be applied to the whole "
"period."
msgstr ""

#: includes/admin/views/wceb-html-product-booking-options.php:24
#: includes/admin/views/wceb-html-product-booking-options.php:37
#: includes/admin/views/wceb-html-product-booking-options.php:54
#: includes/admin/views/wceb-html-product-booking-options.php:70
#: includes/admin/views/wceb-html-product-booking-options.php:86
msgid "Same as global settings"
msgstr ""

#: includes/admin/views/wceb-html-product-booking-options.php:25
#: includes/admin/views/wceb-html-variation-booking-options.php:22
#: includes/settings/class-wceb-settings.php:322
msgid "Daily"
msgstr ""

#: includes/admin/views/wceb-html-product-booking-options.php:26
#: includes/admin/views/wceb-html-variation-booking-options.php:25
#: includes/settings/class-wceb-settings.php:323
msgid "Weekly"
msgstr ""

#: includes/admin/views/wceb-html-product-booking-options.php:27
#: includes/admin/views/wceb-html-variation-booking-options.php:28
#: includes/settings/class-wceb-settings.php:324
msgid "Custom"
msgstr ""

#: includes/admin/views/wceb-html-product-booking-options.php:35
#: includes/admin/views/wceb-html-variation-booking-options.php:36
msgid "Custom booking duration (days)"
msgstr ""

#: includes/admin/views/wceb-html-product-booking-options.php:50
#: includes/admin/views/wceb-html-variation-booking-options.php:46
#: includes/settings/includes/wceb-general-settings.php:54
msgid "Minimum booking duration"
msgstr ""

#: includes/admin/views/wceb-html-product-booking-options.php:52
msgid ""
"The minimum number of days / weeks / custom period to book. Leave zero to "
"set no duration limit. Leave empty to use the global settings."
msgstr ""

#: includes/admin/views/wceb-html-product-booking-options.php:66
#: includes/admin/views/wceb-html-variation-booking-options.php:57
#: includes/settings/includes/wceb-general-settings.php:62
msgid "Maximum booking duration"
msgstr ""

#: includes/admin/views/wceb-html-product-booking-options.php:68
msgid ""
"The maximum number of days / weeks / custom period to book. Leave zero to "
"set no duration limit. Leave empty to use the global settings."
msgstr ""

#: includes/admin/views/wceb-html-product-booking-options.php:82
msgid "First available date (day)"
msgstr ""

#: includes/admin/views/wceb-html-product-booking-options.php:84
msgid ""
"First available date, relative to the current day. I.e. : today + 5 days. "
"Leave zero for the current day. Leave empty to use the global settings."
msgstr ""

#: includes/admin/views/wceb-html-variation-booking-options.php:19
#: includes/admin/views/wceb-html-variation-booking-options.php:39
#: includes/admin/views/wceb-html-variation-booking-options.php:50
#: includes/admin/views/wceb-html-variation-booking-options.php:61
#: includes/admin/views/wceb-html-variation-booking-options.php:72
msgid "Same as parent"
msgstr ""

#: includes/admin/views/wceb-html-variation-booking-options.php:47
msgid ""
"The minimum number of days / weeks / custom period to book. Enter zero to "
"set no duration limit or leave blank to use the parent product's booking "
"options or the global settings."
msgstr ""

#: includes/admin/views/wceb-html-variation-booking-options.php:58
msgid ""
"The maximum number of days / weeks / custom period to book. Enter zero to "
"set no duration limit or leave blank to use the parent product's booking "
"options or the global settings."
msgstr ""

#: includes/admin/views/wceb-html-variation-booking-options.php:68
#: includes/settings/includes/wceb-general-settings.php:70
msgid "First available date"
msgstr ""

#: includes/admin/views/wceb-html-variation-booking-options.php:69
msgid ""
"First available date, relative to today. I.e. : today + 5 days. Enter 0 for "
"today or leave blank to use the parent product's booking options or the "
"global settings."
msgstr ""

#: includes/reports/class-wceb-list-reports.php:24
msgid "Report"
msgstr ""

#: includes/reports/class-wceb-list-reports.php:25
#: includes/reports/class-wceb-reports.php:21
#: includes/reports/class-wceb-reports.php:22
msgid "Reports"
msgstr ""

#: includes/reports/class-wceb-list-reports.php:58
msgid "Order"
msgstr ""

#: includes/reports/class-wceb-list-reports.php:59
msgid "Product"
msgstr ""

#: includes/reports/class-wceb-list-reports.php:62
msgid "Quantity booked"
msgstr ""

#: includes/reports/class-wceb-list-reports.php:114
msgid "Billing:"
msgstr ""

#: includes/reports/class-wceb-list-reports.php:118
msgid "Tel:"
msgstr ""

#: includes/reports/class-wceb-list-reports.php:143
msgid "Guest"
msgstr ""

#: includes/reports/views/html-wceb-reports-filters.php:10
msgid "Search for a product&hellip;"
msgstr ""

#: includes/settings/class-wceb-settings.php:105
#: includes/settings/class-wceb-settings.php:106
msgid "Global Settings"
msgstr ""

#: includes/settings/class-wceb-settings.php:114
#: includes/settings/class-wceb-settings.php:115
#: includes/settings/class-wceb-settings.php:149
#: includes/settings/class-wceb-settings.php:150
msgid "Add-ons"
msgstr ""

#: includes/settings/class-wceb-settings.php:298
msgid ""
"Choose whether to calculate the final price depending on number of days or "
"number of nights (i.e. 5 days = 4 nights)."
msgstr ""

#: includes/settings/class-wceb-settings.php:300
msgid "Days"
msgstr ""

#: includes/settings/class-wceb-settings.php:301
msgid "Nights"
msgstr ""

#: includes/settings/class-wceb-settings.php:310
msgid ""
"Check to make all your products bookable. Any new or modified product will "
"be automatically bookable."
msgstr ""

#: includes/settings/class-wceb-settings.php:334
msgid "Used only for products with \"Custom\" as a booking duration."
msgstr ""

#: includes/settings/class-wceb-settings.php:350
msgid ""
"Set a minimum booking duration for all your products. You can individually "
"change it on your product settings. Leave 0 or empty to set no duration "
"limit."
msgstr ""

#: includes/settings/class-wceb-settings.php:364
msgid ""
"Set a maximum booking duration for all your products. You can individually "
"change it on your product settings. Leave 0 or empty to set no duration "
"limit."
msgstr ""

#: includes/settings/class-wceb-settings.php:377
msgid ""
"Set the first available date for all your products, relative to the current "
"day. You can individually change it on your product settings. Leave 0 or "
"empty to keep the current day."
msgstr ""

#: includes/settings/class-wceb-settings.php:391
msgid ""
"Set the limit to allow bookings (December 31 of year x). Min: current year. "
"Max: current year + 10 years."
msgstr ""

#: includes/settings/class-wceb-settings.php:405
msgid "Monday"
msgstr ""

#: includes/settings/class-wceb-settings.php:406
msgid "Sunday"
msgstr ""

#: includes/settings/class-wceb-settings.php:412
msgid ""
"Make this plugin yours by choosing the different texts you want to display !"
msgstr ""

#: includes/settings/class-wceb-settings.php:420
msgid ""
"Displays an information text before date inputs. Leave empty if you don't "
"want the information text."
msgstr ""

#: includes/settings/class-wceb-settings.php:434
msgid "Text displayed before the first date"
msgstr ""

#: includes/settings/class-wceb-settings.php:444
msgid "Text displayed before the second date"
msgstr ""

#: includes/settings/class-wceb-settings.php:449
msgid "Customize the calendar so it looks great with your theme !"
msgstr ""

#: includes/settings/class-wceb-settings.php:449
msgid "Prefer a light background and a dark text color, for better rendering."
msgstr ""

#: includes/settings/class-wceb-settings.php:458
msgid "Default"
msgstr ""

#: includes/settings/class-wceb-settings.php:459
msgid "Classic"
msgstr ""

#: includes/settings/wceb-settings-functions.php:222
msgid "Add"
msgstr ""

#: includes/settings/wceb-settings-functions.php:262
msgid "Delete"
msgstr ""

#: includes/settings/includes/wceb-appearance-settings.php:15
#: includes/settings/views/html-wceb-settings.php:6
msgid "Appearance"
msgstr ""

#: includes/settings/includes/wceb-appearance-settings.php:22
msgid "Calendar theme"
msgstr ""

#: includes/settings/includes/wceb-appearance-settings.php:30
msgid "Background color"
msgstr ""

#: includes/settings/includes/wceb-appearance-settings.php:38
msgid "Main color"
msgstr ""

#: includes/settings/includes/wceb-appearance-settings.php:46
msgid "Text color"
msgstr ""

#: includes/settings/includes/wceb-general-settings.php:15
msgid "General settings"
msgstr ""

#: includes/settings/includes/wceb-general-settings.php:22
msgid "Calculation mode"
msgstr ""

#: includes/settings/includes/wceb-general-settings.php:30
msgid "Make all products bookable?"
msgstr ""

#: includes/settings/includes/wceb-general-settings.php:46
msgid "Custom booking duration"
msgstr ""

#: includes/settings/includes/wceb-general-settings.php:78
msgid "Booking limit"
msgstr ""

#: includes/settings/includes/wceb-general-settings.php:86
msgid "First weekday"
msgstr ""

#: includes/settings/includes/wceb-text-settings.php:15
msgid "Text settings"
msgstr ""

#: includes/settings/includes/wceb-text-settings.php:22
msgid "Information text"
msgstr ""

#: includes/settings/includes/wceb-text-settings.php:30
msgid "First date title"
msgstr ""

#: includes/settings/includes/wceb-text-settings.php:38
msgid "Second date title"
msgstr ""

#: includes/settings/views/html-wceb-addons.php:8
msgid "WooCommerce Easy Booking Add-ons"
msgstr ""

#: includes/settings/views/html-wceb-addons.php:15
msgid ""
"This add-on will allow you to manage stocks and availabilties when renting "
"or booking your products."
msgstr ""

#: includes/settings/views/html-wceb-addons.php:22
msgid ""
"This add-on will allow you to add discounts to your products depending on "
"the duration booked by your clients."
msgstr ""

#: includes/settings/views/html-wceb-addons.php:30
msgid ""
"This add-on allows you to disable days or date when renting products with "
"WooCommerce Easy Booking."
msgstr ""

#: includes/settings/views/html-wceb-addons.php:54
msgid "Learn more"
msgstr ""

#: includes/settings/views/html-wceb-addons.php:58
msgid "Installed"
msgstr ""

#: includes/settings/views/html-wceb-addons.php:61
msgid "Documentation"
msgstr ""

#: includes/settings/views/html-wceb-addons.php:64
msgid "Support"
msgstr ""

#: includes/settings/views/html-wceb-network-settings.php:5
msgid "Network settings for WooCommerce Easy Booking"
msgstr ""

#: includes/settings/views/html-wceb-settings.php:4
msgid "General "
msgstr ""

#: includes/settings/views/html-wceb-settings.php:5
msgid "Texts"
msgstr ""
