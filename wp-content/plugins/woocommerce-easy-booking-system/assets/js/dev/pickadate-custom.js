(function($) {

	$(document).ready(function() {

		var $inputStart = $('.datepicker_start').pickadate(),
			$inputEnd   = $('.datepicker_end').pickadate();

		// Use the picker object directly.
		var pickerStart = $inputStart.pickadate('picker'),
			pickerEnd   = $inputEnd.pickadate('picker');

		var pickerStartItem = pickerStart.component.item,
			pickerEndItem   = pickerEnd.component.item;

		var selectedDates = {
			startFormat: null,
			endFormat  : null,
			start      : null,
			end        : null
		};

		var productType           = ajax_object.product_type,
			calcMode              = ajax_object.calc_mode, // Days or Nights
			bookingMin            = 1,
			bookingMax            = 0,
			firstDate             = 0,
			bookingDuration       = 0,
			bookingCustomDuration = 0,
			maxYear               = parseInt( ajax_object.max_year ), // Max year
			maxOption             = new Date( maxYear, 11, 31 ), // December 31st of max year
			allowDisabled         = ajax_object.allow_disabled, // Allow disabled dates inside booking period
			session               = false;

		var $pickerWrap = $('.wceb_picker_wrap');

		if ( productType === 'variable' ) {

			$pickerWrap.hide();
			$('.price').find('.wceb-price-format').hide();

			$('body').on( 'found_variation', '.variations_form', function( e, variation ) {

				variationId = variation.variation_id;
				priceHtml   = ajax_object.prices_html[variationId];

				// Clear session
				clearBookingSession();

				if ( ! variation.is_purchasable || ! variation.is_in_stock || ! variation.variation_is_visible || ! variation.is_bookable ) {
					$pickerWrap.hide();
					$('.booking_price').html('');
				} else {
					$pickerWrap.slideDown( 200 );

					var variationPrice = formatPrice( variation.display_price );
					$('.booking_price').html('<span class="price">' + variationPrice + '</span>');
					
					firstDate             = parseInt( ajax_object.first_date[variationId] );
					bookingMin            = parseInt( ajax_object.min[variationId] );
					bookingMax            = ( ajax_object.max[variationId] === '' ) ? '' : parseInt( ajax_object.max[variationId] );
					bookingDuration       = ajax_object.booking_duration[variationId];
					bookingCustomDuration = parseInt( ajax_object.booking_custom_duration[variationId] );

					initPickers();

					$('body').trigger( 'pickers_init', [variation] );

					pickerStart.render();
					pickerEnd.render();

					$('body').trigger( 'after_pickers_init', [variation] );

				}

				// Hide "/ day" or "/ night" if variation is not bookable
				( ! variation.is_bookable ) ? $('.price').find('.wceb-price-format').hide() : $('.price').find('.wceb-price-format').html( priceHtml ).show();

			});

			// Reset variations
			$('body').on('reset_image', '.variations_form', function( e, variation ) {

				$pickerWrap.hide();
				$('.price').find('.wceb-price-format').hide();
				$('.booking_price').html('');

				// Clear session
				clearBookingSession();

			});

		} else if ( productType === 'grouped' ) {

			$pickerWrap.hide();

			var firstDate             = parseInt( ajax_object.first_date ),
				bookingMin            = parseInt( ajax_object.min ),
				bookingMax            = ( ajax_object.max === '' ) ? '' : parseInt( ajax_object.max );
				bookingDuration       = ajax_object.booking_duration;
				bookingCustomDuration = parseInt( ajax_object.booking_custom_duration );

			initPickers();

			$('.cart').on( 'change', '.quantity input.qty', function() {

				var ids        = {},
					quantities = [];

				totalGroupedPrice = 0;

				$.each( $('.quantity input.qty'), function() {
					var qty   = $(this).val(),
						name  = $(this).attr('name'),
						id    = name.split("\[").pop().split("\]").shift(),
						price = ajax_object.product_price[id];

					totalGroupedPrice += parseFloat( price * qty );

					if ( qty > 0 )
						ids[id] = qty;
					
					quantities.push( qty );
				});

				// Get highest quantity selected
				max_qty = Math.max.apply( Math, quantities );

				// Hide date inputs if no quantity is selected
				( max_qty > 0 ) ? $pickerWrap.slideDown( 200 ) : $pickerWrap.hide();

				var formatted_price = formatPrice( totalGroupedPrice );

				$('p.booking_price .price .amount').html( formatted_price );
				
				initPickers();

				$('body').trigger( 'pickers_init', [ids] );

				pickerStart.render();
				pickerEnd.render();
				
				$('body').trigger( 'after_pickers_init', [ids] );

			});
			
		} else { // Simple or other product types

			// Store product booking data
			var firstDate             = parseInt( ajax_object.first_date ),
				bookingMin            = parseInt( ajax_object.min ),
				bookingMax            = ( ajax_object.max === '' ) ? '' : parseInt( ajax_object.max ),
				bookingDuration       = ajax_object.booking_duration,
				bookingCustomDuration = parseInt( ajax_object.booking_custom_duration );

			initPickers();

			$('body').trigger( 'pickers_init' );

			pickerStart.render();
			pickerEnd.render();
			
			$('body').trigger( 'after_pickers_init' );

		}

		/**
		* Check if is date (date object)
		*/
		function isDate( date ) {
			return ( date instanceof Date );
		}

		/**
		* Check if is weekday (1,2,3,4,5,6,7)
		*/
		function isDay( date ) {
			return ( ! isNaN( date ) && ( date >= 1 && date <= 7 ) );
		}

		/**
		* Check if is array ([1,0,2016])
		*/
		function isArray( date ) {
			return ( date instanceof Array );
		}

		/**
		* Check if is an object and not a date (from: [1,0,2016]; to: [1,0,2016])
		*/
		function isObject( date ) {
			return ( ( typeof date === 'object' ) && ! ( date instanceof Date ) );
		}

		/**
		* Init or reset pickers
		*/
		function initPickers() {
			var firstDay    = getFirstAvailableDate( 'start' );
			var endFirstDay = getFirstAvailableDate( 'end' );

			var minObject    = firstDay,
				endMinObject = endFirstDay,
				max          = createDateObject( new Date( maxYear, 11, 31 ) ),
				view         = createDateObject( new Date( minObject.year, minObject.month, 1 ) ),
				endView      = createDateObject( new Date( endMinObject.year, endMinObject.month, 1 ) );

			pickerStartItem.clear     = null;
			pickerStartItem.select    = undefined;
			pickerStartItem.min       = minObject;
			pickerStartItem.max       = max;
			pickerStartItem.highlight = minObject;
			pickerStartItem.view      = view;
			pickerStartItem.disable   = [];

			pickerStart.$node.val('');
			
			pickerEndItem.clear     = null;
			pickerEndItem.select    = undefined;
			pickerEndItem.min       = endMinObject;
			pickerEndItem.max       = max;
			pickerEndItem.highlight = endMinObject;
			pickerEndItem.view      = endView;
			pickerEndItem.disable   = [];

			pickerEnd.$node.val('');

			return false;

		}

		/**
		* Create date object ({date, day, month, object, pick, year})
		*/
		function createDateObject( date, add ) {
			var dateObject = {};

			// If not date, get current date
			if ( ! date ) {
				var date = new Date();
			}

			// Maybe add days
			if ( add ) {
				date.setDate( date.getDate() + add );
			}

			// Create infinity object
			if ( date === 'infinity' ) {
				var dateObject = {
					date : Infinity,
					day  : Infinity,
					month: Infinity,
					obj  : Infinity,
					pick : Infinity,
					year : Infinity
				}

				return dateObject;
			}

			// Check if is valid date
			if ( ! isDate( date ) ) {
				return dateObject;
			}

			// Set date to 00:00
			date.setHours(0,0,0,0);

			// Create date object
			var dateObject = {
				date : date.getDate(),
				day  : date.getDay(),
				month: date.getMonth(),
				obj  : date,
				pick : date.getTime(),
				year : date.getFullYear()
			}

			return dateObject;
		}

		/**
		* Get the first available date
		*/
		function getFirstAvailableDate( picker ) {
			var firstDay = +firstDate;

			if ( picker === 'end' ) {
				firstDay += bookingMin; // Set endpicker start date to first date + minimum duration
			}

			if ( firstDay <= 0 ) {
				var firstDay = false;
			}

			return createDateObject( false, firstDay );

		}

		/**
		* Clear session
		*/
		function clearBookingSession() {

			if ( session ) {

				var data = {
					action: 'clear_booking_session'
				};
			
				$.post( ajax_object.ajax_url, data, function( response ) {
					session = response;
				});
			}

		}

		/**
		* Get min and max from a given date, 'operator' depends on the picker set when the function is called (plus or minus)
		*/
		function getMinAndMax( disabledDate, operator ) {

			var selectedMinDate = new Date( disabledDate.year, disabledDate.month, disabledDate.date ); // Selected date
			var selectedMaxDate = new Date( disabledDate.year, disabledDate.month, disabledDate.date ); // Selected date
			var firstAvailableDate = getFirstAvailableDate( 'start' ); // First available date

			// After setting the end date, if there is no maximum booking duration
			if ( operator === 'minus' && bookingMax === '' ) {
				// Set min to the first available date
				var selectedMinDate = firstAvailableDate.obj;
			}

			// After setting start date
			if ( operator === 'plus' ) {

				selectedMinDate.setDate( selectedMinDate.getDate() + bookingMin );

				if ( bookingMax !== '' ) {
					selectedMaxDate.setDate( selectedMaxDate.getDate() + bookingMax );
				}

			// After setting end date (reverse min and max)
			} else {

				selectedMaxDate.setDate( selectedMaxDate.getDate() - bookingMin );

				// If a maxium booking duration is set
				if ( bookingMax !== '' ) {
					selectedMinDate.setDate( selectedMinDate.getDate() - bookingMax );
				}
				
			}

			// Check if minimum date is not inferior to the first available date
			if ( firstAvailableDate > selectedMinDate ) {
				selectedMinDate = firstAvailableDate; // If it is, set minimum to first available day
			}

			// If no maximum booking duration is set, set it to false
			if ( operator === 'plus' && bookingMax === '' ) {
				selectedMaxDate = false;
			}

			// Set maximum to maximum option (max year) if false
			if ( ! selectedMaxDate || maxOption < selectedMaxDate ) {
				selectedMaxDate = maxOption;
			}

			var minAndMax = {};
     		minAndMax['min'] = selectedMinDate;
     		minAndMax['max'] = selectedMaxDate;

			return minAndMax;

		}

		/**
		* Ajax request to calculate and return price, and store booking session
		*/
		function setPrice( selectedDates ) {
			children = {};

			// Get IDs
			if ( productType === 'variable' ) {
				product_id = $('input[name=product_id]').val();
			} else if ( productType === 'grouped' ) {
				product_id = $('input[name=add-to-cart]').val(),
				productChildren = ajax_object.children;

				$.each(productChildren, function( index, child ) {
					quantity = $('input[name="quantity[' + child + ']"]').val();
					if ( quantity > 0 ) {
						children[child] = quantity;
					}
				});

			} else {
				product_id = $('input[name=add-to-cart]').val();
			}
			
			variation_id = $('.variations_form').find('input[name=variation_id]').val();

			// WooCommerce Product Add-ons compatibility
			var additionalCost = getAdditionalCosts();

			var data = {
				action         : 'add_new_price',
				product_id     : product_id,
				variation_id   : variation_id,
				children       : children,
				start          : selectedDates['start'],
				end            : selectedDates['end'],
				start_format   : selectedDates['startFormat'],
				end_format     : selectedDates['endFormat'],
				additional_cost: additionalCost
			};

			$('form.cart').fadeTo('400', '0.6').block({
				message: null,
				overlayCSS: {
					background: 'transparent',
					backgroundSize: '16px 16px',
					opacity: 0.6
				}
			});

			$.post(ajax_object.ajax_url, data, function( response ) {

				$('.woocommerce-error, .woocommerce-message').remove();
				fragments = response.fragments;
				errors    = response.errors;

				// If error, reset pickers
				if ( errors ) {

					$.each(errors, function(key, value) {
						$(key).replaceWith(value);
					});

					initPickers();
					pickerStart.render();
					pickerEnd.render();

					// Unblock
					$('form.cart').stop(true).css('opacity', '1').unblock();

					return false;

				}

				if ( fragments ) {

					$.each(fragments, function(key, value) {
						$(key).replaceWith(value);
					});

					session = fragments.session;

				}

				$('form.cart').trigger( 'update_price', [ data, response ] );

				// Unblock
				$('form.cart').stop(true).css('opacity', '1').unblock();
			
			});
		}

		/**
		* Get the closest disabled date from a given date, 'direction' depends on the picker set when the function is called ('inferior' or 'superior')
		*/
		function getClosestDisabled( time, picker, direction ) {
			var selectedDate = new Date( time ), // Get Selected date
				selectedDay  = selectedDate.getDay(); // Get selected day (1, 2, 3, 4, 5, 6, 7)

			var disabled     = picker.get('disable'),
				disabledTime = [];

			$.each( disabled, function( index, date ) {

				// [2016, 01, 01]
				if ( isArray( date ) ) {

					if ( date['type'] === 'booked' || allowDisabled === 'no' ) {
						var getDate = new Date( date[0], date[1], date[2] );
						disabledTime.push( getDate.getTime() );
					}

				// { from: [2016, 01, 01], to: [2016, 01, 01] }
				} else if ( isObject( date ) ) {

					if ( direction === 'superior' ) {
						var getDate = new Date( date.from[0], date.from[1], date.from[2] );
					} else {
						var getDate = new Date( date.to[0], date.to[1], date.to[2] );
					}
					
					if ( date.type === 'booked' || allowDisabled === 'no' ) {
						disabledTime.push( getDate.getTime() );
					}

				// Date object
				} else if ( isDate( date ) ) {

					disabledTime.push( date.getTime() );

				// 1, 2, 3, 4, 5, 6, 7
				} else if ( allowDisabled === 'no' && isDay( date ) ) {

					if ( direction === 'superior' ) {

						var interval = Math.abs( selectedDay - date );

						if ( interval === 0 )
							interval = 7;

						if ( date < selectedDay && interval !== 7 )
							interval = 7 - interval;

						var nextDisabledDay = selectedDate.setDate( selectedDate.getDate() + interval );
						
						disabledTime.push( nextDisabledDay );
						selectedDate = new Date( time ); // Reset selected date

					} else if ( direction === 'inferior' ) {

						var interval = Math.abs( selectedDay - date );

						if ( interval === 0 )
							interval = 7;

						if ( selectedDay < date && interval !== 7 )
							interval = 7 - interval;
						
						previousDisabledDay = selectedDate.setDate( selectedDate.getDate() - interval );
						disabledTime.push( previousDisabledDay );

					}

				}

			});
			
			disabledTime.sort();
			var closestDisabled = closest( disabledTime, time, direction );

			return closestDisabled;
		}

		/**
		* Get the closest date from a given date
		*/
		function closest(arr, closestTo, direction) {

			minClosest = false;

		    for ( var i = 0; i < arr.length; i++ ) { // Loop the array
		    	if ( direction === 'superior' ) {
		    		if ( arr[i] > closestTo ) { // Check if it's higher than the date
			    		minClosest = arr[i];
			    		break;
			    	} else {
			    		minClosest = false;
			    	}
		    	} else if ( direction === 'inferior' ) {
		    		if ( arr[i] < closestTo ) { // Check if it's lower than the date
			    		minClosest = arr[i];
			    	}
		    	}
		    }

		    return minClosest;
			
		}

		/**
		* Apply custom booking duration after settings one of the pickers
		*/
		function applyBookingDuration( picker, pickerItem, selected ) {

			var alreadyDisabled = pickerItem.disable, // Get already disabled dates
				thingToCheck    = picker === 'end' ? pickerItem.max : pickerItem.min;

			// Get selected date on the other datepicker
			var selectedDate = new Date( selected.year, selected.month, selected.date );

			// Get last, current and next month duration (in days), relative to the current view
			var view                   = pickerItem.view, // Get current view on the current picker
				lastDayOfPreviousMonth = new Date( view.year, view.month, 0 ).getDate(), // Number of days of last month (relative to view)
				lastDayOfTheMonth      = new Date( view.year, view.month + 1, 0 ).getDate(), // Number of days of viewed month
				lastDayOfNextMonth     = new Date( view.year, view.month + 2, 0 ).getDate(); // Number of days of next month (relative to view)

			// Get the total of days to disable dates (3 months)
			var remainingDays = parseInt( lastDayOfPreviousMonth + lastDayOfTheMonth + lastDayOfNextMonth );

			// Difference between the selected day and the view (in days)
			var diff = Math.abs( Math.round( ( view.pick - selected.pick ) / 86400000 ) );

			// Number of days to start counting
			var diffMinus = picker === 'end' ? parseInt( diff - lastDayOfPreviousMonth ) : parseInt( diff - lastDayOfTheMonth - lastDayOfNextMonth );

			// Number of days to end counting
			var diffPlus = picker === 'end' ? parseInt( diff - lastDayOfPreviousMonth + remainingDays ) : parseInt( diff - lastDayOfNextMonth + remainingDays );
			
			if ( picker === 'end' && diff < lastDayOfPreviousMonth ) {
				var diffMinus = bookingCustomDuration;
				var diffPlus = parseInt( diff + lastDayOfTheMonth + lastDayOfNextMonth );
			}

			if ( picker === 'start' && diffMinus < lastDayOfNextMonth ) {
				var diffMinus = bookingCustomDuration;
				var diffPlus = parseInt( diff + lastDayOfTheMonth + lastDayOfPreviousMonth );
			}

			if ( diffMinus < 0 ) {
				diffMinus = 0;
			}

			if ( diffMinus < bookingCustomDuration ) {
				diffMinus = bookingCustomDuration;
			}

			if ( diffMinus > bookingCustomDuration ) {
				var j;
				var multiples = [];

				// Get the closest multiple of the booking duration
				for ( j = 0; j <= diffMinus; j+= bookingCustomDuration ) {
					multiples.push( j );
				}

				var diffMinus = multiples.slice(-1)[0]; // Get last value
			}

			if ( calcMode === 'days' ) {
				diffMinus -= 1;
			}

			var i;
			var enabled = [1,2,3,4,5,6,7]; // Disable every day

			first = false;

			for ( i = diffMinus; i <= diffPlus; i+=bookingCustomDuration ) {

				var baseSelectedDate = new Date( selected.year, selected.month, selected.date ); // Selected date in the other picker

				if ( picker === 'start' ) {
					baseSelectedDate.setDate( selectedDate.getDate() - i ); // Remove booking duration
				} else if ( picker === 'end' ) {
					baseSelectedDate.setDate( selectedDate.getDate() + i ); // Add booking duration
				}

				dateToEnable = createDateObject( baseSelectedDate );

				// If the date is before the minimum set or after the maximum set, stop
				if ( ( picker === 'end' && dateToEnable.obj > thingToCheck.obj ) || ( picker === 'start' && dateToEnable.obj < thingToCheck.obj ) ) {
					break;
				}

				// Check if the date is disabled
				if ( typeof alreadyDisabled !== 'undefined' && alreadyDisabled.length > 0 ) {
					var d = isDisabled( alreadyDisabled, dateToEnable );
				}

				// If it is disabled, don't enable it
				if ( true === d ) {
					continue;
				}
				
				enabled.push( [dateToEnable.year, dateToEnable.month, dateToEnable.date, 'inverted'] ); // add 'inverted' to enable date
			}

			pickerItem.disable = alreadyDisabled.concat( enabled ); // Merge arrays

			return false;
		}

		/**
		* Check if the date is disabled
		*/
		function isDisabled( disabled, dateToEnable ) {

			if ( typeof disabled === 'undefined' ) {
				return false;
			}

		 	var d = false;

		 	var timeToEnable = dateToEnable.pick;

			$.each( disabled, function( index, dateObject ) {

				// [year, month, day]
				if ( isArray( dateObject ) ) { 

					dateObject = new Date( dateObject[0], dateObject[1], dateObject[2] );

					if ( timeToEnable === dateObject.getTime() ) {
						d = true;
						return;
					}

				// { from: [year, month, day], to: [year, month, day] }
				} else if ( isObject( dateObject ) ) {

					start = new Date( dateObject['from'][0], dateObject['from'][1], dateObject['from'][2] );
					end   = new Date( dateObject['to'][0], dateObject['to'][1], dateObject['to'][2] );

					if ( timeToEnable >= start.getTime() && timeToEnable <= end.getTime() ) {
						d = true;
						return;
					}

				// 1, 2, 3, 4, 5, 6, 7
				} else if ( isDay( dateObject ) ) { 

					var day = dateToEnable.day;

					if ( day === 0 ) {
						day = 7;
					}

					if ( dateObject === day ) {
						d = true;
						return;
					}

				// Date object
				} else if ( isDate( dateObject ) ) { 

					if ( timeToEnable === dateObject.getTime() ) {
						d = true;
						return;
					}

				}

			});

			return d;
		}

		/**
		* Clear the other picker
		*/
		function clearSecondPicker( picker, secondPicker, secondPickerObject ) {

			var secondPickerItem = secondPickerObject.component.item,
				min  = getFirstAvailableDate( secondPicker ),
				max  = createDateObject( maxOption ),
				view = createDateObject( new Date( min.year,min.month,01 ) );

			secondPickerItem.disable = [];
			secondPickerItem.min     = min;
			secondPickerItem.max     = max;

			if ( secondPickerObject.get('select') === null ) {
				secondPickerItem.highlight = min;
				secondPickerItem.view      = view;
			}

			$('body').trigger( 'clear_' + picker + '_picker', secondPickerItem );

			secondPickerObject.render();
		}

		/**
		* Set the other picker and call Ajax function if both pickers are set
		*/
		function setPickers( picker, pickerObject, secondPickerObject, secondPickerItem ) {
			var selectedObject       = pickerObject.get('select'), // Array [year,month,date,day,obj,pick]
				secondSelectedObject = secondPickerObject.get('select'); // Array [year,month,date,day,obj,pick]

			if ( selectedObject === null ) {
				return;
			}

			var direction = picker === 'start' ? 'superior' : 'inferior',
				calc      = picker === 'start' ? 'plus' : 'minus';

			var selectedTimestamp = selectedObject.pick; // Unix timestamp
			
			selectedDates[picker + 'Format'] = pickerObject.get('select', 'yyyy-mm-dd'), // yyyy-mm-dd
			selectedDates[picker]            = pickerObject.get('select', 'dd mmmm yyyy'); // Nice format

			var minAndMax = getMinAndMax( selectedObject, calc ),
				min       = minAndMax.min,
				max       = minAndMax.max;

			var thingToSet = picker === 'start' ? max : min;

			// If no maximum date is set, set max to maximum year
			if ( ! max ) {
				max = maxOption;
			}

			// Get the closest disabled date
			var closestDisabled = getClosestDisabled( selectedTimestamp, secondPickerObject, direction );

			// If a date is disabled
			if ( closestDisabled ) {

				var date = new Date( closestDisabled ); // Convert to date

				if ( ( picker === 'start' && closestDisabled < thingToSet ) || ( picker === 'end' && closestDisabled > thingToSet ) ) {
					var thingToSet = date;
				}
			}

			var min  = picker === 'start' ? createDateObject( min ) : createDateObject( thingToSet ),
				max  = picker === 'start' ? createDateObject( thingToSet ) : createDateObject( max ),
				view = createDateObject( new Date( min.year,min.month,01 ) );

			secondPickerItem.min  = min;
			secondPickerItem.max  = max;
			secondPickerItem.view = view;

			// If other picker is not set
			if ( typeof secondSelectedObject === 'undefined' || secondSelectedObject === null ) {
				secondPickerItem.highlight = min;
			}

			$('body').trigger('set_' + picker + '_picker', [secondPickerItem, selectedTimestamp] );

			secondPickerObject.render();

			// If both pickers are set
			if ( typeof selectedObject !== 'undefined' && selectedObject !== null && typeof secondSelectedObject !== 'undefined'&& secondSelectedObject !== null  ) {
				// Ajax request to calculate price and store session data
				setPrice( selectedDates );
			}
		}

		/**
		* Open the second picker when selecting a date
		*/
		function closePicker( picker, secondPicker ) {
			// Bug fix
			$( document.activeElement ).blur();

			var thisSet   = picker.get('select'),
				secondSet = secondPicker.get('select');

			// Open other picker if current picker is set and other not
			if ( ( thisSet !== null && typeof thisSet !== 'undefined' ) && ( secondSet === null || typeof secondSet === 'undefined' ) ) {
				setTimeout( function() { secondPicker.open(); }, 250 );
			}
		}

		/**
		* Format price
		*/
		function formatPrice( price ) {
			formatted_price = accounting.formatMoney( price, {
				symbol 		: ajax_object.currency_format_symbol,
				decimal 	: ajax_object.currency_format_decimal_sep,
				thousand	: ajax_object.currency_format_thousand_sep,
				precision 	: ajax_object.currency_format_num_decimals,
				format		: ajax_object.currency_format
			} );

			return formatted_price;
		}

		/**
		* WooCommerce Product Add-ons compatibility
		*/
		function getAdditionalCosts() {

			var total = 0;

			$('form.cart').find( '.addon' ).each( function() {
				var addon_cost = 0;

				if ( $(this).is('.addon-custom-price') ) {
					addon_cost = $(this).val();
				} else if ( $(this).is('.addon-input_multiplier') ) {
					if( isNaN( $(this).val() ) || $(this).val() == "" ) { // Number inputs return blank when invalid
						$(this).val('');
						$(this).closest('p').find('.addon-alert').show();
					} else {
						if( $(this).val() != "" ){
							$(this).val( Math.ceil( $(this).val() ) );
						}
						$(this).closest('p').find('.addon-alert').hide();
					}
					addon_cost = $(this).data('price') * $(this).val();
				} else if ( $(this).is('.addon-checkbox, .addon-radio') ) {
					if ( $(this).is(':checked') )
						addon_cost = $(this).data('price');
				} else if ( $(this).is('.addon-select') ) {
					if ( $(this).val() )
						addon_cost = $(this).find('option:selected').data('price');
				} else {
					if ( $(this).val() )
						addon_cost = $(this).data('price');
				}

				if ( ! addon_cost )
					addon_cost = 0;

				total += addon_cost;
			});

			return total;

		}

		/**
		* Check if a date is set
		*/
		function dateIsSet( date ) {
			return ( typeof date !== 'undefined' && date !== null );
		}

		/**
		* Check if both dates (start and end) are set
		*/
		function datesAreSet() {
			var startSelected = pickerStart.get('select'),
				endSelected   = pickerEnd.get('select');

			return ( ( startSelected !== null && typeof startSelected !== 'undefined' ) && ( endSelected !== null && typeof endSelected !== 'undefined' ) );
		}

		pickerStart.on({
			render: function() {

				// Display picker title
				pickerStart.$root.find('.picker__header').prepend('<div class="picker__title">' + ajax_object.start_text + '</div>');

			},
			set: function( startTime ) {

				// If picker is cleared
				if ( typeof startTime.clear !== 'undefined' && startTime.clear === null ) {

					// Reset min, max and disabled dates on other picker
					clearSecondPicker( 'start', 'end', pickerEnd );

					selectedDates['start']       = null;
					selectedDates['startFormat'] = null;

					// Clear session
					clearBookingSession();
				}

				// If picker is set
				if ( dateIsSet( startTime.select ) ) {
					setPickers( 'start', pickerStart, pickerEnd, pickerEndItem );
				}
				
			},
			close: function() {
				closePicker( pickerStart, pickerEnd );
			}
		});

		pickerEnd.on({
			render: function() {

				// Display picker title
				pickerEnd.$root.find('.picker__header').prepend('<div class="picker__title">' + ajax_object.end_text + '</div>');
				
			},
			set: function( endTime ) {

				// If picker is cleared
				if ( typeof endTime.clear !== 'undefined' && endTime.clear === null ) {

					// Reset min, max and disabled dates on other picker
					clearSecondPicker( 'end', 'start', pickerStart );

					selectedDates['end']       = null;
					selectedDates['endFormat'] = null;

					// Clear session
					clearBookingSession();
				}

				// If picker is set
				if ( dateIsSet( endTime.select ) ) {
					setPickers( 'end', pickerEnd, pickerStart, pickerStartItem );
				}

				return false;
				
			},
			close: function() {
				closePicker( pickerEnd, pickerStart );
			}
		});

		/**
		* Before rendering the start picker
		*/
		pickerStart.on( 'before_rendering', function() {

			var selected = pickerEnd.get('select'); // Get selected date on the End picker

			startPickerDisabled = pickerStartItem.disable; // Store already disabled dates

			if ( dateIsSet( selected ) && bookingDuration !== 'days' && bookingCustomDuration > 1  ) {
				applyBookingDuration( 'start', pickerStartItem, selected );
			}

		});

		/**
		* After rendering the start picker
		*/
		pickerStart.on( 'after_rendering', function() {
			pickerStartItem.disable = startPickerDisabled; // Reset disabled dates
		});

		/**
		* Before rendering the end picker
		*/
		pickerEnd.on( 'before_rendering', function() {

			var selected = pickerStart.get('select'); // Get selected date on the Start picker

			endPickerDisabled = pickerEndItem.disable; // Store already disabled dates

			if ( dateIsSet( selected ) && bookingDuration !== 'days' && bookingCustomDuration > 1 ) {
				applyBookingDuration( 'end', pickerEndItem, selected );
			}

		});

		/**
		* After rendering the end picker
		*/
		pickerEnd.on( 'after_rendering', function() {
			pickerEndItem.disable = endPickerDisabled; // Reset disabled dates
		});

		/**
		* WooCommerce Product Add-ons compatibility
		*/
		$('body').on('updated_addons', function() {

			var product_price = 0;
			
			if ( productType === 'variable' ) {
				var variationId   = $('.variations_form').find('input[name=variation_id]').val(),
					product_price = parseFloat( ajax_object.product_price[variationId] );
			} else if ( productType === 'grouped') {
				var product_price = totalGroupedPrice;
			} else {
				var product_price = parseFloat( ajax_object.product_price );
			}

			var addon_costs = getAdditionalCosts();

			if ( ! addon_costs )
				return;
			
			var total_price = parseFloat( addon_costs + product_price );

			if ( addon_costs > 0 ) {
				pickerStart.clear();
				pickerEnd.clear();
			}

			var formatted_total = formatPrice( total_price );
			
			$('p.booking_price .price .amount').html( formatted_total );
			
		});

	});
})(jQuery);