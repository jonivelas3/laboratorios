(function($) {

	$(document).ready(function() {

		$.extend( $.fn.pickadate.defaults, {
			formatSubmit: 'yyyy-mm-dd',
			hiddenName  : true
		});

		var format = $.fn.pickadate.defaults.format;

		var item_picker = $('#woocommerce-order-items').on( 'click', 'a.edit-order-item', function() {

			var line            = $(this).closest( 'tr' );
			var datepickerInput = line.find( '.datepicker' );
			var startHidden     = line.find( '.start_display' );
			var endHidden       = line.find( '.end_display' );
			var id              = line.find( '.variation_id' );

			var $input = datepickerInput.pickadate();

			if ( datepickerInput.length && id.length ) {

				setStartOnLoad = false;
				setEndOnLoad   = false;

				var item_id     = id.data( 'item_id' );
				var $inputStart = $( '.datepicker_start--' + item_id ).pickadate();
				var $inputEnd   = $( '.datepicker_end--' + item_id ).pickadate();
				var pickerStart = $inputStart.pickadate( 'picker' );
				var pickerEnd   = $inputEnd.pickadate( 'picker' );

				setStart = $( '.datepicker_start--' + item_id ).data( 'value' ),
				setEnd   = $( '.datepicker_end--' + item_id ).data( 'value' );

				pickerStart.on({
					set: function(startTime) {

						if ( typeof startTime.clear != 'undefined' && startTime.clear == null ) {

							pickerEnd.set( 'min', false, { muted: true } );
							startHidden.val('');

						} else if ( startTime.select && typeof startTime.select != 'undefined' ) {

							startFormat = pickerStart.get( 'select', format );
							startHidden.val( startFormat );

							startPickerData = pickerStart.get( 'select' );

							if ( order_ajax_info.calc_mode === 'days' ) {

								pickerEnd.set(
									'min',
									[startPickerData.year, startPickerData.month, startPickerData.date],
									{ muted: true }
								);

							} else {

								pickerEnd.set(
									'min',
									[startPickerData.year, startPickerData.month, startPickerData.date + 1],
									{ muted: true }
								);

							}

							if ( setStart == '' ) {
								setStartOnLoad = true;
							}

						}
						
					}
				});

				pickerEnd.on({
					set: function( endTime ) {

						if ( typeof endTime.clear != 'undefined' && endTime.clear == null ) {

							pickerStart.set( 'max', false, { muted: true } );
							endHidden.val('');

						} else if ( endTime.select && typeof endTime.select != 'undefined' ) {

							endFormat = pickerEnd.get( 'select', format );
							endHidden.val( endFormat );

							endPickerData = pickerEnd.get( 'select' );

							if ( order_ajax_info.calc_mode === 'days' ) {

								pickerStart.set(
									'max',
									[endPickerData.year, endPickerData.month, endPickerData.date],
									{ muted: true }
								);

							} else {

								pickerStart.set(
									'max',
									[endPickerData.year, endPickerData.month, endPickerData.date - 1],
									{ muted: true }
								);

							}

							if ( setEnd == '' ) {
								setEndOnLoad = true;
							}

						}
						
					}
				});

				if ( setStart != '' ) {
					pickerStart.set( 'select', setStart );
				}

				if ( setEnd != '' ) {
					pickerEnd.set( 'select', setEnd );
				}

			}

		});

	});

})(jQuery);