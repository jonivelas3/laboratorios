<?php

if ( ! defined( 'ABSPATH' ) ) {
    exit; // Exit if accessed directly
}

if ( ! class_exists( 'WCEB_Order' ) ) :

class WCEB_Order {

    public function __construct() {

        $this->options = get_option( 'easy_booking_settings' );

        add_action( 'woocommerce_before_order_itemmeta', array( $this, 'easy_booking_order_display_product_dates' ), 10, 3 );

    }
    /**
    *
    * Displays booked dates and a picker form on the order page
    *
    * @param int $item_id
    * @param object $item
    * @param WC_Product $_product
    *
    **/
    public function easy_booking_order_display_product_dates( $item_id, $item, $_product ) {

        $product_id   = $item['product_id'];
        $variation_id = $item['variation_id'];

        $start_date_set = wc_get_order_item_meta( $item_id, '_ebs_start_format' );
        $end_date_set   = wc_get_order_item_meta( $item_id, '_ebs_end_format' );

        $start_date_text = $this->options['easy_booking_start_date_text'];
        $end_date_text   = $this->options['easy_booking_end_date_text'];

        if ( ! empty( $start_date_set ) && ! empty( $end_date_set ) ) {

            $meta_data = $item['item_meta_array'];

            foreach ( $item['item_meta_array'] as $meta_id => $meta ) {

                switch ( $meta->key ) {
                    case '_ebs_start_format':
                        $start_meta_id = esc_attr( $meta_id );
                    break;
                    case '_ebs_start_display':
                        $start_display_meta_id = esc_attr( $meta_id );
                    break;
                    case '_ebs_end_format':
                        $end_meta_id = esc_attr( $meta_id );
                    break;
                    case '_ebs_end_display':
                        $end_display_meta_id = esc_attr( $meta_id );
                    break;
                }

            }

            // Product or variation ID
            $id      = empty( $variation_id ) ? $product_id : $variation_id;
            $product = wc_get_product( $id );

            // If ordered before 1.4, dates will be displayed in English
            $start_date = empty( $item['ebs_start_display'] ) ? date('d F Y', strtotime( $start_date_set ) ) : $item['ebs_start_display'];
            $end_date   = empty( $item['ebs_end_display'] ) ? date('d F Y', strtotime( $end_date_set ) ) : $item['ebs_end_display'];

            include( 'views/html-wceb-order-item-meta.php' );

            include( 'views/html-wceb-edit-order-item-meta.php' );

        }

    }

}

return new WCEB_Order();

endif;