<?php

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

if ( ! class_exists( 'WCEB_Ajax' ) ) :

class WCEB_Ajax {

    private $options;
    private $tax_display_mode;

	public function __construct() {
        // Get plugin options values
        $this->options          = get_option('easy_booking_settings');
        $this->tax_display_mode = get_option('woocommerce_tax_display_shop');
        
		add_action( 'wp_ajax_add_new_price', array( $this, 'wceb_get_new_price' ) );
        add_action( 'wp_ajax_nopriv_add_new_price', array( $this, 'wceb_get_new_price' ) );
        add_action( 'wp_ajax_clear_booking_session', array( $this, 'wceb_clear_booking_session' ) );
        add_action( 'wp_ajax_nopriv_clear_booking_session', array( $this, 'wceb_clear_booking_session' ) );
        add_action( 'wp_ajax_woocommerce_get_refreshed_fragments', array( $this, 'wceb_new_price_fragment' ) );
        add_action( 'wp_ajax_nopriv_woocommerce_get_refreshed_fragments',  array( $this, 'wceb_new_price_fragment' ) );
        add_action( 'wp_ajax_wceb_hide_notice', array( $this, 'wceb_hide_notice' ) );
        add_action( 'wp_ajax_wceb_reports_product_id', array( $this, 'wceb_reports_product_id' ) );
	}

    /**
    *
    * Calculates new price, update product meta and refresh fragments
    *
    **/
    public function wceb_get_new_price() {
        $product_id   = isset( $_POST['product_id'] ) ? absint( $_POST['product_id'] ) : ''; // Product ID
        $variation_id = isset( $_POST['variation_id'] ) ? absint( $_POST['variation_id'] ) : ''; // Variation ID
        $children     = isset( $_POST['children'] ) ? array_map( 'absint', $_POST['children'] ) : array(); // Product children for grouped and variable products

        $id = ! empty( $variation_id ) ? $variation_id : $product_id; // Product or variation id

        $calc_mode = $this->options['easy_booking_calc_mode']; // Calculation mode (Days or Nights)

        $start_date = isset( $_POST['start'] ) ? sanitize_text_field( $_POST['start'] ) : ''; // Booking start date
        $end_date   = isset( $_POST['end'] ) ? sanitize_text_field( $_POST['end'] ) : ''; // Booking end date

        // If one date is empty
        if ( empty( $start_date ) || empty( $end_date ) ) {
            $error_code = 2;
        }

        $start = isset( $_POST['start_format'] ) ? sanitize_text_field( $_POST['start_format'] ) : ''; // Booking start date 'yyyy-mm-dd'
        $end   = isset( $_POST['end_format'] ) ? sanitize_text_field( $_POST['end_format'] ) : ''; // Booking end date 'yyyy-mm-dd'

        $additional_cost = isset( $_POST['additional_cost'] ) ? wc_format_decimal( $_POST['additional_cost'], 2 ) : 0;

        $start_time = strtotime( $start );
        $end_time   = strtotime( $end );

        if ( $end_time < $start_time ) {
            $error_code = 1;
        }

        // Get booking duration
        $diff = absint( $start_time - $end_time );
        $days = $diff / 86400;

        if ( $days == 0 ) {
            $days = 1;
        }

        $product  = wc_get_product( $product_id ); // Product object
        $_product = wc_get_product( $id ); // Product or variation object

        $duration         = absint( $days );
        $booking_duration = wceb_get_product_booking_duration( $_product );

        // If booking mode is days and calculation mode is set to "Days", add one day
        if ( $calc_mode === 'days' && ( $start != $end ) ) {
            $duration = absint( $days + 1 );
        }

        // If booking mode is weeks and duration is a multiple of 7
        if ( $booking_duration === 'weeks' ) {

            if ( $calc_mode === 'nights' && $duration % 7 === 0 ) { // If in weeks mode, check that the duration is a multiple of 7
                $duration = absint( $duration / 7 );
            } else if ( $calc_mode === 'days' && $duration % 6 === 0 ) { // Or 6 in "Days" mode
                $duration = absint( $duration / 6 );
            } else { // Otherwise throw an error
                $error_code = 1;
            }
            
        } else if ( $booking_duration === 'custom' ) {

            $custom_booking_duration = wceb_get_product_custom_booking_duration( $_product );

            if ( $duration % $custom_booking_duration === 0 ) {
                $duration = absint( $duration / $custom_booking_duration );
            } else {
                $error_code = 1;
            }

        }

        // If number of days is inferior to 0
        if ( $duration <= 0 ) {
            $error_code = 1;
        }
        
        $prices_include_tax = get_option( 'woocommerce_prices_include_tax' );
        $tax_display_mode = get_option( 'woocommerce_tax_display_shop' );
        
        // If no variation was selected
        if ( $product->is_type( 'variable' ) && empty( $variation_id ) ) {
            $error_code = 3;
        }

        // If no quantity was selected for grouped products
        if ( $product->is_type( 'grouped' ) && empty( $children ) ) {
            $error_code = 4;
        }

        // Show error message
        if ( isset( $error_code ) ) {

            $error_message = $this->wceb_get_date_error( $error_code );
            wc_add_notice( $error_message, 'error' );

            $this->wceb_error_fragment( $error_message );
            $this->wceb_clear_booking_session();
            die();

        }
        
        // Get additional costs including or excluding taxes (for WooCommerce Product Addons)
        if ( $additional_cost > 0 ) {

            if ( $_product->is_taxable() ) {

                $rates = WC_Tax::get_base_tax_rates( $_product->get_tax_class() );

                if ( $prices_include_tax === 'yes' && $tax_display_mode === 'excl' ) {

                    $taxes = WC_Tax::calc_exclusive_tax( $additional_cost, $rates );

                    if ( $taxes ) foreach ( $taxes as $tax ) {
                        $additional_cost += $tax;
                    }

                } else if ( $prices_include_tax === 'no' && $tax_display_mode === 'incl' ) {

                   $taxes = WC_Tax::calc_inclusive_tax( $additional_cost, $rates );

                   if ( $taxes ) foreach ( $taxes as $tax ) {
                        $additional_cost -= $tax;
                    }

                }

            }
            
        }

        $data = array(
            'duration'   => $duration,
            'start_date' => $start_date,
            'end_date'   => $end_date,
            'start'      => $start,
            'end'        => $end
        );

        $booking_data  = array();
        $booking_price = 0;

        if ( $product->is_type( 'grouped' ) && ! empty( $children ) ) {

            foreach ( $children as $child_id => $quantity ) {

                $child    = wc_get_product( $child_id );

                $children_price[$child_id] = $prices_include_tax === 'yes' ? $child->get_price_including_tax() : $child->get_price_excluding_tax(); // Product price (Regular or sale)

                $child_booking_price       = wc_format_decimal( $children_price[$child_id] * $duration ); // Price for x days
                $child_new_price           = apply_filters( 'easy_booking_get_new_grouped_item_price', $child_booking_price, $product, $child, $duration );

                $data['new_price']       = $child_new_price;
                $booking_data[$child_id] = $data;

                $booking_price += ( $child_new_price * $quantity );

            }

            // No additionnal costs as WooCommerce Product Add-ons doesn't work with grouped products.

        } else {

            $price         = $prices_include_tax === 'yes' ? $_product->get_price_including_tax() : $_product->get_price_excluding_tax(); // Product price (Regular or sale) 
            $booking_price = wc_format_decimal( ( $price + $additional_cost ) * $duration );

        }

        $new_price = apply_filters( 'easy_booking_get_new_item_price', $booking_price, $product, $_product, $duration ); // Price for x days
        
        $data['new_price'] = $new_price;
        $booking_data[$id] = $data;

        // Update session data
        WC()->session->set_customer_session_cookie( true );
        WC()->session->set( 'booking', $booking_data );

        // Return fragments
        $this->wceb_new_price_fragment();

        die();

    }

    /**
    *
    * Clears session if "Clear" button is clicked on the calendar
    *
    **/
    public function wceb_clear_booking_session() {
        $session = WC()->session->get( 'booking' );

        if ( ! empty( $session ) ) {
            WC()->session->set( 'booking', '' );
        }

        $session_set = false;

        die( $session_set );
    }

    /**
    *
    * Gets error messages
    *
    * @param int $error_code
    * @return str $err - Error message
    *
    **/
    private function wceb_get_date_error( $error_code ) {

        switch ( $error_code ) {
            case 1:
                $err = __( 'Please choose valid dates', 'easy_booking' );
            break;
            case 2:
                $err = __( 'Please choose two dates', 'easy_booking' );
            break;
            case 3:
                $err = __( 'Please select product option', 'easy_booking' );
            break;
            case 4:
                $err = __( 'Please choose the quantity of items you wish to add to your cart&hellip;', 'woocommerce' );
            break;
            default:
                $err = '';
            break;
        }

        return $err;
    }

    /**
    *
    * Updates error messages with Ajax
    *
    * @param str $error_message
    *
    **/
    public function wceb_error_fragment( $error_message ) {

        header( 'Content-Type: application/json; charset=utf-8' );

        ob_start();
        wc_print_notices();
        $error_message = ob_get_clean();

            $data = array(
                'errors' => array(
                    'div.wc_ebs_errors' => '<div class="wc_ebs_errors">' . $error_message . '</div>'
                )
            );

        wp_send_json( $data );

        die();

    }

    /**
    *
    * Updates price fragment
    *
    **/
    public function wceb_new_price_fragment() {

        header( 'Content-Type: application/json; charset=utf-8' );

        $session = false;
        
        $product_id   = isset( $_POST['product_id'] ) ? absint( $_POST['product_id'] ) : '';
        $variation_id = isset( $_POST['variation_id'] ) ? absint( $_POST['variation_id'] ) : '';
        $id           = empty( $variation_id ) ? $product_id : $variation_id;

        $product = wc_get_product( $id );

        if ( ! $product )
            return;

        $booking_session = WC()->session->get( 'booking' );
        $new_price       = $booking_session[$id]['new_price']; // New booking price

        $tax_display_mode = get_option( 'woocommerce_tax_display_shop' );

        $new_price = $tax_display_mode === 'incl' ? $product->get_price_including_tax( 1, $new_price ) : $product->get_price_excluding_tax( 1, $new_price ); // Product price (Regular or sale)

        $currency = apply_filters( 'easy_booking_currency', get_woocommerce_currency_symbol() ); // Currency
        $args     = apply_filters( 'easy_booking_new_price_args', array() );
        
        if ( ! empty( $booking_session[$id]['duration'] ) ) {

            $session = true;

            ob_start();
            $fragments = ob_get_clean();

                $data = array(
                    'fragments' => apply_filters( 'easy_booking_fragments', array(
                        'p.booking_price>span.price' => '<p class="booking_price"><span class="price">' . wc_price( $new_price, $args ) . '</span></p>',
                        'session' => $session
                        )
                    )
                );

            wp_send_json( $data );
            die();
        }

    }

    /**
    *
    * Hide notices after clicking on the "close" button
    *
    **/
    public function wceb_hide_notice() {
        $notice = isset( $_POST['notice'] ) ? sanitize_text_field( $_POST['notice'] ) : '';

        if ( get_option( 'easy_booking_display_notice_' . $notice ) !== '1' ) {
            update_option( 'easy_booking_display_notice_' . $notice, '1' );
        }

        die();
    }

    /**
    *
    * Gets filtered products on the reports page
    *
    **/
    public static function wceb_reports_product_id() {
        ob_start();

        $term = (string) wc_clean( stripslashes( $_GET['term'] ) );

        $booked_products = wceb_get_booked_items_from_orders();

        $found_products = array();
        if ( $booked_products ) foreach ( $booked_products as $booked_product ) {
            $product_id   = $booked_product['product_id'];
            $product      = wc_get_product( $product_id );
            $product_name = get_the_title( $product_id );

            if ( ! $product ) {
                continue;
            }
            
            if ( stristr( $product_name, $term ) !== FALSE) {
                $found_products[ $product_id ] = $product->get_formatted_name();
            }
        }

        wp_send_json( $found_products );
        die();

    }
}

return new WCEB_Ajax();

endif;