<?php

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
} ?>

<div class="wrap">
	<h2><?php _e('WooCommerce Easy Booking Add-ons'); ?></h2>
	<div class="addons-container row">
		<?php $addons = array(
			array(
				'name' => 'Easy Booking: Availability Check',
				'slug' => 'easy-booking-availability-check',
				'desc' => '<p>'
					. __( 'This add-on will allow you to manage stocks and availabilties when renting or booking your products.', 'easy_booking' ) .
				'</p>'
			),
			array(
				'name' => 'Easy Booking: Duration Discounts',
				'slug' => 'easy-booking-duration-discounts',
				'desc' => '<p>
					' .  __( 'This add-on will allow you to add discounts to your products depending on the duration booked by your clients.', 'easy_booking' ) . '
				</p>'
			)
			,
			array(
				'name' => 'Easy Booking: Disable Dates',
				'slug' => 'easy-booking-disable-dates',
				'desc' => '<p>
					' .  __( 'This add-on allows you to disable days or date when renting products with WooCommerce Easy Booking.', 'easy_booking' ) . '
				</p>'
			)
		);

		$plugin_dir = plugins_url( '/', WCEB_PLUGIN_FILE );

		$active_plugins = (array) get_option( 'active_plugins', array() );

        if ( is_multisite() ) {
            $active_plugins = array_merge( $active_plugins, get_site_option( 'active_sitewide_plugins', array() ) );
        }

		foreach ( $addons as $addon ) : ?>
			<div class="addon-single">
				<div class="addon-single__img">
					<img src="<?php echo $plugin_dir . 'assets/img/addons/' . $addon['slug'] . '.png'; ?>" alt="<?php echo $addon['slug']; ?>">
				</div>
				<div class="addon-single__desc">
					<h2><?php echo $addon['name']; ?></h2>
					<?php echo $addon['desc']; ?>
					<p>
						<?php if ( ! ( array_key_exists( $addon['slug'] .'/' . $addon['slug'] . '.php', $active_plugins ) || in_array( $addon['slug'] .'/' . $addon['slug'] . '.php', $active_plugins ) ) ) { ?>
						<a href="http://herownsweetcode.com/product/<?php echo $addon['slug']; ?>" target="_blank" class="button">
							<?php _e('Learn more', 'easy_booking'); ?>
						</a>
						<?php } else { ?>
						<a href="#" class="button easy-booking-button easy-booking-button--installed">
							<?php _e('Installed', 'easy_booking'); ?>
						</a>
						<a href="http://herownsweetcode.com/product/<?php echo $addon['slug']; ?>/#faq" target="_blank" class="button">
							<?php _e('Documentation', 'easy_booking'); ?>
						</a>
						<a href="http://herownsweetcode.com/support/<?php echo $addon['slug']; ?>" target="_blank" class="button">
							<?php _e('Support', 'easy_booking'); ?>
						</a>
						<?php } ?>
					</p>
				</div>
			</div>

		<?php endforeach; ?>
	</div>
</div>