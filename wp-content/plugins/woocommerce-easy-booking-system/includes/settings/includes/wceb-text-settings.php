<?php

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

register_setting(
	'easy_booking_text_settings',
	'easy_booking_settings', 
	array( $this, 'sanitize_values' )
);

add_settings_section(
	'easy_booking_main_text',
	__( 'Text settings', 'easy_booking' ),
	array( $this, 'easy_booking_section_text' ),
	'easy_booking_text_settings'
);

add_settings_field(
	'easy_booking_info_text',
	__( 'Information text', 'easy_booking' ),
	array( $this, 'easy_booking_info' ),
	'easy_booking_text_settings',
	'easy_booking_main_text'
);

add_settings_field(
	'easy_booking_start_date_text',
	__( 'First date title', 'easy_booking' ),
	array( $this, 'easy_booking_start_date' ),
	'easy_booking_text_settings',
	'easy_booking_main_text'
);

add_settings_field(
	'easy_booking_end_date_text',
	__( 'Second date title', 'easy_booking' ),
	array( $this, 'easy_booking_end_date' ),
	'easy_booking_text_settings',
	'easy_booking_main_text'
);