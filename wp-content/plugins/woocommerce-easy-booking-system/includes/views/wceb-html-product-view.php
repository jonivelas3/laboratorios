<!-- Display info text -->
<?php if ( isset( $info_text ) && ! empty ( $info_text ) ) { ?>
    <p class="woocommerce-info wceb_picker_wrap"><?php echo esc_html( $info_text ); ?></p>
<?php }?>

<div class="wc_ebs_errors"><?php wc_print_notices(); ?></div>

<!-- Do not remove existing inputs' attributes (classes, ids, etc.) -->
<div class="wceb_picker_wrap">
    <p class="form-row form-row-wide">
        <label for="start_date"><?php echo esc_html( $start_date_text ); ?></label>
        <input type="hidden" id="variation_id" name="variation_id" data-product_id="<?php echo absint( $product->id ) ?>" value="">
        <input type="text" id="start_date" class="datepicker datepicker_start" data-value="" placeholder="<?php echo esc_attr( $start_date_text ); ?>">
    </p>
    <p class="form-row form-row-wide">
        <label for="end_date"><?php echo esc_html( $end_date_text ); ?></label>
        <input type="text" id="end_date" class="datepicker datepicker_end" data-value="" placeholder="<?php echo esc_attr( $end_date_text ); ?>">
    </p>
</div>

<p class="booking_price">
    <!-- If the product is variable, the price will be loaded with Javascript for each variation -->
	<?php echo $product->is_type( 'variable' ) ? '' : '<span class="price">' . wc_price( $product_price, $args ) . '</span>' ?>
</p>