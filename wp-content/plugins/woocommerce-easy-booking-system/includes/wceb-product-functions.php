<?php

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

/**
*
* Returns whether the product is bookable or not
* @param mixed $product - Product object or product ID
* @return bool
*
**/
function wceb_is_bookable( $product ) {

	// If product ID was passed, get the product
	if ( is_numeric( $product ) ) {
		$product = wc_get_product( $product );
	}

	if ( ! $product ) {
		return false;
	}

	$product_type = $product->product_type;

	// Check the product type
	$allowed_product_types   = WCEB()->allowed_types;
	$allowed_product_types[] = 'variation';

	return ( $product->booking_option === 'yes' && in_array( $product_type, $allowed_product_types ) ) ? true : false;

}

/**
*
* Returns the product booking data
* If the data is empty, it gets the global booking setting
*
* @param WC_Product $product - Product object
* @return array - $booking_data
*
**/
function wceb_get_product_booking_data( $product ) {

	// Get plugin settings
	$plugin_settings = get_option('easy_booking_settings');
    $calc_mode       = $plugin_settings['easy_booking_calc_mode'];

	$product_id = $product->id;

	// If a setting is empty, get the global setting instead
	$booking_meta = array(
    	'booking_min',
    	'booking_max',
    	'first_available_date'
    );

	foreach ( $booking_meta as $meta ) {

		$global_meta = $plugin_settings['easy_booking_' . $meta];

		if ( ! isset( $product->$meta ) || ( empty( $product->$meta ) && $product->$meta != '0' ) ) {
			${$meta} = $global_meta;
		} else {
			${$meta} = $product->$meta;
		}

	}

	if ( $booking_min <= 0 ) {
		$booking_min = 1;
	}

	// Get custom booking duration
	$custom_booking_duration = wceb_get_product_custom_booking_duration( $product );

	// Multiply by custom booking duration
	$booking_min = $calc_mode === 'days' ? $booking_min * $custom_booking_duration - 1 : $booking_min * $custom_booking_duration;
	$booking_max = $calc_mode === 'days' ? $booking_max * $custom_booking_duration - 1 : $booking_max * $custom_booking_duration;

	if ( $booking_min <= 0 ) {
		$booking_min = 0;
	}

	if ( ( $calc_mode === 'days' && $booking_max < 0 ) || ( $calc_mode === 'nights' && $booking_max <= 0 ) ) {
		$booking_max = false;
	}

	$booking_data = array(
		'booking_duration'        => wceb_get_product_booking_duration( $product ),
		'custom_booking_duration' => $custom_booking_duration,
		'booking_min'             => $booking_min,
		'booking_max'             => $booking_max,
		'first_available_date'    => $first_available_date
	);

	return $booking_data;

}

/**
*
* Returns the variation booking data
* If the data is empty, it gets the parent product booking setting (or the global settings)
*
* @param WC_Product_Variation - $variation - Variation object
* @param array - $parent_booking_data - The parent product booking data
* @return array - $booking_data
*
**/
function wceb_get_variation_booking_data( $variation, array $parent_booking_data ) {

	if ( ! $variation ) {
		return;
	}

	// Get plugin settings
	$plugin_settings = get_option('easy_booking_settings');
    $calc_mode       = $plugin_settings['easy_booking_calc_mode'];

	$variation_id = $variation->variation_id;

	// If a setting is empty, get the parent setting instead
	$booking_meta = array(
    	'booking_min',
    	'booking_max',
    	'first_available_date'
    );

	foreach ( $booking_meta as $meta ) {

		if ( ! isset( $variation->$meta ) || ( empty( $variation->$meta ) && $variation->$meta != '0' ) ) {
			${$meta} = $parent_booking_data[$meta];
		} else {
			${$meta} = $variation->$meta;
		}
	}

	if ( $booking_min <= 0 ) {
		$booking_min = 1;
	}

	// Get custom booking duration
	$custom_booking_duration = wceb_get_product_custom_booking_duration( $variation );

	// Multiply by custom booking duration
	$booking_min = $calc_mode === 'days' ? $booking_min * $custom_booking_duration - 1 : $booking_min * $custom_booking_duration;
	$booking_max = $calc_mode === 'days' ? $booking_max * $custom_booking_duration - 1 : $booking_max * $custom_booking_duration;

	if ( $booking_min <= 0 ) {
		$booking_min = 0;
	}

	if ( ( $calc_mode === 'days' && $booking_max < 0 ) || ( $calc_mode === 'nights' && $booking_max <= 0 ) ) {
		$booking_max = false;
	}

	$booking_data = array(
		'booking_duration'        => wceb_get_product_booking_duration( $variation ),
		'custom_booking_duration' => $custom_booking_duration,
		'booking_min'             => $booking_min,
		'booking_max'             => $booking_max,
		'first_available_date'    => $first_available_date
	);

	return $booking_data;

}

/**
*
* Gets the html to display the booking price on the product page
*
* @param WC_Product or WC_Product_Variation $product - The product or variation object
* @return str - $price_html
*
**/
function wceb_get_price_html( $product ) {

	$plugin_settings  = get_option('easy_booking_settings');
    $calc_mode        = $plugin_settings['easy_booking_calc_mode'];

	// Get product booking duration
    $booking_duration = wceb_get_product_booking_duration( $product );

	// Get product custom booking duration
	$custom_duration = wceb_get_product_custom_booking_duration( $product );

    // If it is a variable product, price format will be displayed in Javascript for each variation
    if ( $product->is_type( 'variable' ) || ! wceb_is_bookable( $product ) ) {

        $price_html = '';

    } else {

        if ( $booking_duration === 'weeks' ) {

            $price_html = __(' / week', 'easy_booking');

        } else if ( $booking_duration === 'custom' && $custom_duration != '1' ) {

        	if ( $calc_mode === 'nights' ) {
        		$price_html = sprintf( _n( ' / %s night', ' / %s nights', $custom_duration, 'easy_booking' ), $custom_duration );
        	} else {
        		$price_html = sprintf( _n( ' / %s day', ' / %s days', $custom_duration, 'easy_booking' ), $custom_duration );
        	}

        } else if ( $calc_mode === 'nights' ) {

            $price_html = __(' / night', 'easy_booking');

        } else {

            $price_html = __(' / day', 'easy_booking');

        }

    }

    return apply_filters( 'easy_booking_get_price_html', $price_html, $product, $booking_duration, $custom_duration );
}

/**
*
* Gets product booking duration
*
* @param WC_Product or WC_Product_Variation $product - The product or variation object
* @return str - $booking_duration
*
**/
function wceb_get_product_booking_duration( $product ) {

	$plugin_settings  = get_option('easy_booking_settings');
    $product_id       = $product->is_type( 'variation' ) ? $product->variation_id : $product->id;
    $booking_duration = get_post_meta( $product_id, '_booking_duration', true );

    // If it is a children grouped product on the parent product page
    if ( $product->get_parent() && get_queried_object_id() !== $product_id ) {
    	$booking_duration = get_post_meta( $product->get_parent(), '_booking_duration', true );
    }

    // If it is a variation with the parent product setting
    if ( $product->is_type( 'variation' ) && ( $booking_duration === 'parent' || empty( $booking_duration ) ) ) {
    	$booking_duration = get_post_meta( $product->parent->id, '_booking_duration', true );
    }

    if ( empty( $booking_duration ) || $booking_duration === 'global' ) {
    	$booking_duration = $plugin_settings['easy_booking_duration'];
    }

    return $booking_duration;

}

/**
*
* Gets product custom booking duration
*
* @param WC_Product or WC_Product_Variation $product - The product or variation object
* @return int - $custom_booking_duration
*
**/
function wceb_get_product_custom_booking_duration( $product ) {

    $product_id = $product->is_type( 'variation' ) ? $product->variation_id : $product->id;

    $plugin_settings  = get_option('easy_booking_settings');
    $calc_mode        = $plugin_settings['easy_booking_calc_mode'];

    $custom_booking_duration = get_post_meta( $product_id, '_custom_booking_duration', true );

    // If it is a children grouped product on the parent product page
    if ( $product->get_parent() && get_queried_object_id() !== $product_id ) {
    	$custom_booking_duration = get_post_meta( $product->get_parent(), '_custom_booking_duration', true );
    }

    // If it is a variation with the parent product setting
    if ( $product->is_type( 'variation' ) && ( empty( $custom_booking_duration ) ) ) {
    	$custom_booking_duration = get_post_meta( $product->parent->id, '_custom_booking_duration', true );
    }

    if ( empty( $custom_booking_duration ) || $custom_booking_duration === 'global' ) {
    	$custom_booking_duration = $plugin_settings['easy_booking_custom_duration'];
    }

    $booking_duration = wceb_get_product_booking_duration( $product );

    // If booking duration is "Weeks" and the plugin in "Days" mode, a week is 6 days
    if ( $booking_duration === 'weeks' && $calc_mode === 'days' ) {
        $custom_booking_duration = 6;
    } else if ( $booking_duration === 'weeks' && $calc_mode === 'nights' ) {
        $custom_booking_duration = 7;
    }

    if ( $custom_booking_duration <= 0 ) {
    	$custom_booking_duration = 1;
    }

    return $custom_booking_duration;

}